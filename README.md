# Kuro Bot v.0.4.1-indev

###### A Discord bot created by Vitobru and Alatar

**Info:** This is a small Discord bot written sitting on top of the discord.py library. Still in its alpha stages of development, it can perform some basic tasks such as latency, uptime, greeting, and playing uploaded mp3 files in VC, all prefaced by the $ parameter (soon to be customisable).

**Prerequisites:** You're gonna need `discord.py`, pickledb, PyNaCl, redis, and youtube_dl for this; all of which you can easily install through the terminal, using:
```
pip install -U discord.py
pip install -U pickledb
pip install -U youtube_dl
pip install -U PyNaCl
pip install -U redis
```

Then, you'll need to install ffmpeg and redis however you can.
On Arch Linux, it's:
```
pacman -S ffmpeg
pacman -S redis
```

Please note that Windows is not supported, you will need to fix the issues yourself.

**Team:**

Lead Programmers - Vitobru, armeabiv7

Support & QA - alatartheblue42



**Contact:** [BElISLEMusic@outlook.com](BElISLEMusic@outlook.com) if you wanna email me about the progress. Otherwise, just tweet me [@vitobrutweet](https://twitter.com/vitobrutweet/).
